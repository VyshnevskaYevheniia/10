package week1;

public class TenProgram {
    public static void main(String args[]) {
        int[] arr = {1, 5, 33, 12, 88, 9, 192, 123, 567, 88, 44, 32};

        for (int curr_arr : arr) {
            if (curr_arr > 10)
                System.out.println(curr_arr);
        }
    }

}
